;(function () {
  'use strict';

  angular.module('LJ.RadioGroup', [])
  /**
   * Allows to work with radio groups.
   * When model with some id is set to true, all others will be set to false.
   *
   * @return {Object}   factory
   * @return {Function} factory.get  RadioGroup getter
   *
   * @example
   *
   *    // get RadioGroup instance
   *    var editable = radioGroup('editable');
   *
   *    // transfer models to scope
   *    $scope.editable = editable.models();
   *
   *    editable.on(1); // turn on editable with ID #1
   *    editable.on(2); // turn on editable with ID #2,
   *                    // editable with ID #1 will be turned off automatically
   *
   *    editable.off(2);       // turn off editable with ID #2
   *    editable.set(1, true); // turn on  editable with ID #1
   */
  .factory('radioGroup', [function () {
    // radio groups cache
    var _cache = {};

    function RadioGroup() {
      this._models = {};
    }

    /**
     * Returns models
     * @return {Object} Models object
     */
    RadioGroup.prototype.models = function () {
      return this._models;
    };

    /**
     * Set all models to false state
     * @return {RadioGroup}  Self instance
     */
    RadioGroup.prototype.reset = function () {
      var models = this._models,
          id;

      for (id in models) {
        if ( models.hasOwnProperty(id) && models[id]) {
          this._models[id] = false;
        }
      }

      return this;
    };

    /**
     * Set model value
     *
     * @param  {*} id          Identifier
     * @param  {Boolean} value Value to set
     * @return {RadioGroup}    Self instance
     */
    RadioGroup.prototype.set = function (id, value) {
      if (value) {
        this.reset()._models[id] = true;
      } else {
        this._models[id] = false;
      }

      return this;
    };

    /**
     * Set model with provided ID to true
     *
     * @param  {*} id       ID value
     * @return {RadioGroup} Self instance
     */
    RadioGroup.prototype.on = function (id) {
      return this.set(id, true);
    };

    /**
     * Set model with provided ID to false
     *
     * @param  {*} [id]     ID value. If not provided - turn off all
     * @return {RadioGroup} Self instance
     */
    RadioGroup.prototype.off = function (id) {
      if (typeof id === 'undefined') {
        this.reset();
      } else {
        this.set(id, false);
      }

      return this;
    };

    /**
     * Get instance of RadioGroup object
     *
     * @param  {String}     name RadioGroup identifier
     * @return {RadioGroup}      RadioGroup instance
     */
    function get(name) {
      if ( _cache[name] ) {
        return _cache[name];
      }

      _cache[name] = new RadioGroup();
      return _cache[name];
    }

    return get;
  }]);
}());

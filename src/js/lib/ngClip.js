/*global ZeroClipboard*/
angular.module('ngClipboard', [])
  .directive('clipCopy', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      compile: function (element, attrs) {
        var get = $parse(attrs.clipCopy);

        return function link(scope, element, attrs) {
          // Create the clip object
          var client = new ZeroClipboard(element);

          client.clip(element);

          client.on('dataRequested', function (client) {
            client.setText( get(scope) );
            scope.$apply(attrs.ngClick);
          });

          scope.$on('$destroy', function () {
            client.off('dataRequested');
          });
        };
      }
    };
  }]);

/*global ZeroClipboard*/
ZeroClipboard.config({
  moviePath: '/ZeroClipboard.swf',
  cacheBust: false
});

angular.module('CS', ['infinite-scroll', 'ui.bootstrap', 'ngClipboard', 'LJ.RadioGroup'])
  .filter('filterServers', [function () {
    return function (servers, filter) {
      var map = filter.map.trim();

      if ( !Array.isArray(servers) ) {
        return;
      }

      servers = servers.filter(function (server) {

        // @todo filter only if provided map really exist
        if ( map.length !== 0 && server.map !== map ) {
          return false;
        }

        if ( server.players.now < filter.players.min ||
             server.players.now > filter.players.max ) {

          return false;
        }

        return true;
      });

      return servers;
    };
  }])
  .controller('AppCtrl', ['$scope', '$http', '$timeout', '$interval', '$location', 'radioGroup',
                function ( $scope,   $http,   $timeout,   $interval,   $location,   radioGroup ) {

    var copied = radioGroup('copied');

    $scope.copied = copied.reset().models();

    function getData() {
      $http.get('data.json').success(function (data) {
        $scope.servers = data;

        $scope.maps = data.map(function (server) {
          return server.map;
        }).reduce(function (result, map) {
          if ( result.indexOf(map) === -1 ) {
            result.push(map);
          }

          return result;
        }, []).sort();
      });
    }

    getData();
    $interval(getData, 60000);

    $scope.limit = 100;
    $scope.loadMore = function () {
      $scope.limit += 100;
    };

    // init filter
    $scope.filter = {
      map: $location.search().map || '',
      players: {
        min: angular.isDefined($location.search().min) ?
             Number($location.search().min) :
             0,
        max: angular.isDefined($location.search().max) ?
             Number($location.search().max) :
             32,
      }
    };

    $scope.$watch('filter.players.min', function (value, old) {
      if (value === old) {
        return;
      }

      if (value > $scope.filter.players.max) {
        $scope.filter.players.min = $scope.filter.players.max;
      }

      $location.search('min', $scope.filter.players.min);
    });

    $scope.$watch('filter.players.max', function (value, old) {
      if (value === old) {
        return;
      }

      if (value < $scope.filter.players.min) {
        $scope.filter.players.max = $scope.filter.players.min;
      }

      $location.search('max', $scope.filter.players.max);
    });

    $scope.$watch('filter.map', function (value, old) {
      if (value === old) {
        return;
      }

      $location.search('map', value);
    });

    $scope.copy = function (index) {
      copied.on(index);
      $timeout(function () {
        copied.off();
      }, 500);
    };
  }]);

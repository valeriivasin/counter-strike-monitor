module.exports = function (grunt) {
  'use strict';

  grunt.loadNpmTasks('grunt-node-deploy');
  grunt.loadNpmTasks('grunt-preprocess');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.initConfig({
    connect: {
      server: {
        options: {
          port: 8000,
          base: 'public'
        }
      }
    },

    watch: {
      scripts: {
        files: 'src/**/*.*',
        tasks: ['dev'],
        options: {
          livereload: true
        }
      },
    },

    shell: {
      options: {
        stdout: true
      },
      clean: {
        command: 'rm -rf public/*; true;'
      },
      copy: {
        command: ['css', 'js', 'img', 'bower_components'].map(function (folder) {
          return 'cp -r src/' + folder + ' public/';
        }).join(';')
      },
      copySWF: {
        command: [
          'cp',
          'src/bower_components/zeroclipboard/ZeroClipboard.swf',
          'public/ZeroClipboard.swf'
        ].join(' '),
      }
    },

    uglify: {
      min: {
        options: {
          sourceMap: true,
          sourceMapName: 'public/js/app.map'
        },
        files: {
          'public/js/app.min.js': [
            'src/bower_components/zeroclipboard/ZeroClipboard.min.js',
            'src/js/lib/*.js',
            'src/js/app.js'
          ]
        }
      }
    },

    cssmin: {
      combine: {
        files: {
          'public/css/app.min.css': [
            'src/css/flags.css'
          ]
        }
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'src/img/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'public/img/'
        }]
      }
    },

    preprocess : {
      prod : {
        src : 'src/index.grunt.html',
        dest : 'public/index.html',
        options: {
          context : {
            env: 'production'
          }
        }
      },

      dev : {
        src : '<%= preprocess.prod.src %>',
        dest : '<%= preprocess.prod.dest %>',
        options: {
          context : {
            env: 'development'
          }
        }
      },
    },

    deploy: {
      production: {
        deployTo: '/opt/www/cs.valeriivasin.com',

        repository: 'root@valeriivasin.com:/opt/git/cs.git',
        branch:     'master',

        user:   'root',
        domain: 'valeriivasin.com',

        tasks: {
          beforeNpm: function (run) {
            // symlink node_modules
            run('mkdir -p {{sharedPath}}/node_modules');
            run('rm -f {{latestRelease}}/node_modules');
            run('ln -s {{sharedPath}}/node_modules {{latestRelease}}/node_modules');
          },

          afterNpm: function (run) {
            run('cd {{latestRelease}}; bower install --allow-root;');

            // copy servers.json and data.json from previous release
            run('cp {{previousRelease}}/public/data.json {{latestRelease}}/public/data.json || true');
            run('cp {{previousRelease}}/servers.json {{latestRelease}}/servers.json || true');

            // build
            run('cd {{latestRelease}}; grunt build');
          },

          restart: function (run) {
            run('/etc/init.d/nginx restart');
          }
        }
      }
    }
  });

  grunt.registerTask('default', ['preprocess']);
  grunt.registerTask('build', ['shell:clean', 'preprocess:prod', 'uglify', 'cssmin', 'imagemin', 'shell:copySWF']);
  grunt.registerTask('build:dev', ['shell:copy', 'shell:copySWF', 'preprocess:dev']);
  grunt.registerTask('dev', ['build:dev', 'connect', 'watch']);
};

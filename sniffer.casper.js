/**
 * casperjs sniffer.casper.js
 */
var casper = require('casper').create({
  clientScripts: [
    'jquery.min.js'
  ]
});
var fs = require('fs');
var config = require('./config');

casper.start();

var servers = [];
var Strategies = {
  csmonitor: {
    base: 'http://cs-monitor.ru/',

    run: function () {
      var that = this;

      casper.thenOpen(this.base, function () {
        var pages = this.evaluate(function () {
              return Number( $('.pages a').eq(-2).text() );
            }),
            i;

        for (i = 1; i <= pages; i += 1) {
          that.parsePage(i);
        }
      });
    },

    parsePage: function (page) {
      if ( page !== 1 ) {
        casper.thenOpen( [this.base, 'cs/', page, '/'].join('') );
      }

      casper.then(function () {
        var pageServers = casper.evaluate(function () {
          return $('.monitoring .row1').map(function () {
            return $(this).find('td').eq(3).text();
          }).toArray();
        });

        servers = servers.concat(pageServers);

        console.log('page: ', page);
        console.log('servers on the page: ', pageServers.length);
        console.log('total servers: ', servers.length);
      });
    }
  },

  csmonitoring: {
    base: 'http://cs-monitoring.ru/',

    run: function () {
      var that = this;

      casper.thenOpen(this.base, function () {
        var pages = casper.evaluate(that.evaluatePagesAmount),
            i;

        for (i = 1; i <= pages; i += 1) {
          (function (i) {
            casper.thenOpen(this.base + 'index.php?page=' + i, function () {
              var pageServers = this.evaluate( that.evaluateServers );

              servers = servers.concat(pageServers);

              // info
              console.log('page: ', i);
              console.log('servers on the page: ', pageServers.length);
              console.log('total servers: ', servers.length);
            });
          }).call(that, i);
        }
      });
    },

    /**
     * Evaluating servers on page script
     * @return {Array} Array of server addresses
     */
    evaluateServers: function () {
      return $('tr.tiitem').map(function () {
        return $(this).find('td:eq(1)').text().trim();
      }).toArray();
    },

    /**
     * Evaluating pages amount script
     * @return {Number} Amount of pages
     */
    evaluatePagesAmount: function () {
      return Number( $('.navigation > a:last').text() );
    }
  }
};

Strategies.csmonitor.run();
Strategies.csmonitoring.run();

casper.then(saveServers);

function saveServers() {
  // get uniq servers
  var uniq = {},
      uniqServers = [];

  servers.forEach(function (address) {
    if ( !uniq[address] ) {
      uniqServers.push(address);
      uniq[address] = true;
    }
  });

  // filter empty servers
  uniqServers = uniqServers.filter(function (address) {
    return address.trim().length > 0;
  });

  console.log('Servers sniffed: ', servers.length);
  console.log('Uniq servers: ', uniqServers.length);

  fs.write(
    config.SERVERS_FILENAME,
    JSON.stringify(uniqServers, null, '  '),
    'w'
  );
}

casper.run();

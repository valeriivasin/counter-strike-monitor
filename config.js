module.exports = {
  // parsed servers
  SERVERS_FILENAME: 'servers.json',

  // servers information for display
  SERVERS_DATA_FILENAME: 'public/data.json'
};

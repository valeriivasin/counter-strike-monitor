/**
 * Get servers meta information
 * node schedule.js
 * node schedule.js --once
 */
var Gamedig = require('gamedig');
var async   = require('async');
var fs      = require('fs');
var args    = process.argv.slice(2);
var runOnce = args[0] === '--once';

var config = require('./config');

function getStats(address, callback) {
  var info = address.split(':'),
      host = info[0],
      port = Number(info[1]);

  Gamedig.query({
    type: 'cscz',
    host: host,
    port: port
  }, function (state) {
    if (state.error) {
      callback();
      console.error('Error: ' + state.error);
      return;
    }

    callback(null, {
      address: address,
      title:   state.name,
      map:     state.map,
      players: {
        total: state.maxplayers,
        now:   state.players.length
      }
    });
  });
}

var addresses = JSON.parse(
  fs.readFileSync(config.SERVERS_FILENAME, 'utf-8')
);

function run() {
  console.time('update time');

  async.mapLimit(addresses, 10, getStats, function (err, results) {
    if (err) {
      console.log('Error: ', err);
    }

    // filter errors
    results = results.filter(Boolean);

    // update time
    if ( fs.existsSync(config.SERVERS_DATA_FILENAME) ) {
      var hash = JSON.parse(
        fs.readFileSync(config.SERVERS_DATA_FILENAME)
      ).reduce(function (prev, server) {
        prev[server.address] = server;
        return prev;
      }, {});

      var now = Date.now();
      results.forEach(function (server) {
        var address = server.address;

        if ( hash[address] && hash[address].map === server.map ) {
          server.timestamp = hash[address].timestamp ? hash[address].timestamp : now;
        } else {
          server.timestamp = now;
        }

        // in minutes
        server.time = Math.floor( (now - server.timestamp) / 60000) + 1;
      });
    }

    fs.writeFileSync(
      configSERVERS_DATA_FILENAME,
      JSON.stringify(results, null, '  '),
      'utf-8'
    );

    console.log('%d of %d servers info updated.', results.length, addresses.length);
    console.timeEnd('update time');

    if ( !runOnce ) {
      run();
    }
  });
}

run();
